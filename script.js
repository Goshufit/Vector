(function () {
    const profileUrl = 'https://vector-crud-616d3-default-rtdb.firebaseio.com/.json';

    class ProfileOne {
        constructor(
            id = "",
            firstName,
            jobTitle
        ) {
            this.id = id !== this.id ? this.randomId() : id,
                this.firstName = firstName,
                this.jobTitle = jobTitle
        }
        static profileUrl() {
            return `https://vector-crud-616d3-default-rtdb.firebaseio.com/${id}/.json`
        }

        generateTable() {
            const {
                id,
                firstName,
                jobTitle
            } = this;
            console.log('this:', this)
            console.log({ id, firstName, jobTitle })
            return `
            <tr>
                <td>
                    <input class="form-check-input-dark mt-0" type="checkbox" id="accept" onclick="turnGreen(this)"
                    aria-label="Checkbox for following text input">
                </td>

                <td>${firstName}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>${jobTitle}</td>
            </tr>
            `;
        };

        randomId() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        };

        static async deleteProfile(id) {
            await fetch(ProfileOne.profileUrl(id), {
                method: 'DELETE'
            })
        };


        async addProfile() {
            const {
                id,
                firstName,
                jobTitle
            } = this;

            const body = JSON.stringify({
                firstName,
                jobTitle
            });

            const putResponse = await fetch(this.profileUrl(id), {
                method: 'PUT',
                body
            });

        };
    };

    const getFormDataAndSubmit = async () => {
        const profileObj = {
            firstName: firstName.value,
            lastName: lastName.value,
            startDate: startDate.value,
            lastUpdated: lastUpdated.value,
            jobTitle: jobTitle.value
        }

        const customerDataTable = document.querySelector('.customerData');
        const newProfile = new ProfileOne(profileObj);
        await newProfile.addProfile();
        customerDataTable.innerHTML += newProfile.generateTable();
    };

    const startApp = async () => {
        let customerDataTable = document.querySelector('.customerData')
        const row = document.querySelector('tr')
        const response = await fetch(profileUrl);
        const data = await response.json();
        Object.keys(data).forEach(id => {
            const customerData = data[id]
            customerDataTable.innerHTML += new ProfileOne(customerData.id, customerData.firstName, customerData.jobTitle).generateTable();
        });
        row.addEventListener('click', ({ target: {
            className,
            nodeName,
            dataset: {
                profileId
            }
        }
        }) => {
            if (nodeName === 'BUTTON') {
                const isSuccessButton = className.includes('success');
                const isDangerButton = className.includes('danger');

                if (isSuccessButton) {
                    console.log(`Success button clicked! ${profileId}`)
                } else if (isDangerButton) {
                    ProfileOne.deleteProfile(profileId);
                }
            }
        })
    }


    startApp();
    const btnSave = document.querySelector('#btnSave')
    btnSave.addEventListener('click', getFormDataAndSubmit);

})();
const callData = async () => {
    try {
        const response = await fetch('https://vector-crud-616d3-default-rtdb.firebaseio.com/.json');
        const response1 = await fetch('https://vector-crud-2-default-rtdb.firebaseio.com/.json');
        const response2 = await fetch('https://vector-crud3-default-rtdb.firebaseio.com/.json');
        const data = await response.json();
        const data1 = await response1.json();
        const data2 = await response2.json();
        console.log('Success', data)
        console.log('Success', data1)
        console.log('Success', data2)
    }
    catch (err) {
        console.log('Fail', err)
    }
}
callData();


const turnGreen = (checkBox) => {
    const createBtn = document.querySelector('#createBTN')
    const allCheckboxes = document.querySelectorAll('.form-check-input-dark')
    console.log(allCheckboxes)
    let checkBoxesChecked
    allCheckboxes.forEach((checkBox) => {
        if (checkBox.checked) {
            checkBoxesChecked = true;
            checkBox.parentNode.parentNode.classList.add('table-success')
        } else {
            checkBox.parentNode.parentNode.classList.remove('table-success')
        }
    })
    if (checkBoxesChecked) {

        createBtn.style.display = 'none'
    } else {
        createBtn.style.display = 'inline-block'
    }

};



// const deleteEntry = async (id) => {
//     try {
//         await fetch(`https://vector-crud-616d3-default-rtdb.firebaseio.com/${id}/.json`, {
//             method: 'DELETE'
//         })
//     }
//     catch (err) {
//         console.log('Fail');
//     }
// }

// const callData = async () => {
//     try {
//         const response = await fetch('https://vector-crud-616d3-default-rtdb.firebaseio.com/.json');
//         const response1 = await fetch('https://vector-crud-2-default-rtdb.firebaseio.com/.json');
//         const response2 = await fetch('https://vector-crud3-default-rtdb.firebaseio.com/.json');
//         const data = await response.json();
//         const data1 = await response1.json();
//         const data2 = await response2.json();
//         console.log('Success', data)
//         console.log('Success', data1)
//         console.log('Success', data2)
//     }
//     catch (err) {
//         console.log('Fail', err)
//     }

// }

// const createEntries = async () => {
//     const db1 = {
//         "id": "1",
//         "firstName": "monkey",
//         "jobTitle": "what?"
//     }

//     const db2 = {
//         "id": "1",
//         "lastName": "",
//         "lastUpdated": ""
//     }

//     const db3 = {
//         "id": "1",
//         "startDate": ""
//     }

//     await fetch('https://vector-crud-616d3-default-rtdb.firebaseio.com/1/.json', { method: "POST", body: JSON.stringify(db1)} );
//     await fetch('https://vector-crud-2-default-rtdb.firebaseio.com/1/.json', { method: "POST", body: JSON.stringify(db2)} );
//     await fetch('https://vector-crud3-default-rtdb.firebaseio.com/1/.json', { method: "POST", body: JSON.stringify(db3)} );

// }

// createEntries()


// const turnGreen = () => {
//     const cb = document.querySelector('#accept');
//     const btn = document.querySelector('.form-check-input-dark');
//     btn.onclick = () => {
//        alert(cb.value);
//        console.log(value);
//     };

// }
// const btn = document.querySelector('#accept')
// btn.addEventListener('click', turnGreen(() => {
//     console.log(value)
// }));

// callData();
// deleteEntry(1);


// class ProfileTwo {
//     constructor({
//         id,
//         lastUpdated,
//         lastName
//     }) {
//         this.id = id;
//         this.lastName = lastName,
//             this.lastUpdated = lastUpdated
//     }

//     generateTable() {
//         const {
//             id,
//             lastName,
//             lastUpdated
//         } = this;
//         return `
//         <tr>
//         <td><input class="form-check-input-dark mt-0" type="checkbox" id="accept" onclick="turnGreen(this)"
//         aria-label="Checkbox for following text input"></td>
//         <td></td>
//         <td>${lastName}</td>
//         <td></td>
//         <td>${lastUpdated}</td>
//         <td></td>
//         </tr>
//         `;
//     }
// }
// class ProfileThree {
//     constructor({
//         id,
//         startDate
//     }) {
//         this.id = id;
//         this.startDate = startDate

//     }

//     generateTable() {
//         const {
//             id,
//             startDate
//         } = this;
//         return `
//         <tr>
//         <td><input class="form-check-input-dark mt-0" type="checkbox" id="accept" onclick="turnGreen(this)"
//         aria-label="Checkbox for following text input"></td>
//         <td></td>
//         <td></td>
//         <td>${startDate}</td>
//         <td></td>
//         <td></td>
//         </tr>
//         `;
//     }
// }
